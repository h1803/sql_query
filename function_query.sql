-- Создание функции для поиска по комбинации тема + под_тема

create
or replace function get_books (
pattern_theme varchar, pattern_sub_theme varchar
)
returns table (
title varchar,
author varchar,
cover_image varchar,
price numeric
)
language plpgsql
as $$
begin
return query
SELECT b.title, a.author, b.image_url, b.price
FROM books b
         JOIN authors a on (a.id = b.author_id)
         JOIN sub_themes s on (s.id = b.sub_theme_id)
         JOIN themes t on (t.id = s.main_theme_id)
WHERE theme = pattern_theme
  AND sub_theme = pattern_sub_theme;
end;$$



-- Запрос - выбрать количество книг по издателю

SELECT publisher AS publisher_name, count(*)
FROM books b
         JOIN publishers p ON (b.publisher_id = p.id)
GROUP BY publisher;


-- Выбрать количество книг по языкам c процентами

WITH c AS (SELECT language, count (*) AS count
FROM books b
    JOIN languages l
ON (b.language_id = l.id)
GROUP BY language
    )
SELECT *,
       round(100.0 * count / (SELECT sum(count) FROM c), 2) AS percent
FROM c;
